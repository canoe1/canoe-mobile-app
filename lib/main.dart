import 'package:canoemobile/view/basket_page.dart';
import 'package:canoemobile/view/favorites.dart';
import 'package:canoemobile/view/flight_page.dart';
import 'package:canoemobile/view/follow_page.dart';
import 'package:canoemobile/view/product_page.dart';
import 'package:canoemobile/view/reverse_page.dart';
import 'package:canoemobile/view/search.dart';
import 'package:canoemobile/view/signup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'entity/productPageArgument.dart';
import 'entity/user.dart';
import 'view/login.dart';

void main(){
  DotEnv().load('.env');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final appTitle = 'CANOE';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Color(0xFFFF6A0F),
        scaffoldBackgroundColor: Color(0xFF515151),
        backgroundColor: Color(0xFF515151),
        cardColor: Color(0xFFF8F8F8),
        canvasColor: Color(0xFF515151),
        buttonTheme: ButtonThemeData(
          buttonColor: Color(0xFFFF6A0F),
        ),
        accentColor: Color(0xFFFF6A0F),
        hintColor: Color(0xFFABABAB),
        fontFamily: 'Bourton',

        textTheme: TextTheme(
          subtitle1: TextStyle(color: Colors.black),
          subtitle2: TextStyle(color: Colors.black),

          headline1: TextStyle(color: Colors.black),
          headline2: TextStyle(color: Colors.black),
          headline3: TextStyle(color: Colors.black),
          headline4: TextStyle(color: Colors.black),
          headline5: TextStyle(color: Colors.black),
          headline6: TextStyle(color: Colors.black),

        ),
      ),
      home: MyHomePage(title: appTitle),
      onGenerateRoute: (settings) {
        switch(settings.name){
          case '/product':
            final ProductPageArgument args = settings.arguments;
            return MaterialPageRoute(
              builder: (context) => new ProductPage(
                productPageArgument: args,
              ),
            );

          case '/login':
            final args = settings.arguments;
            return MaterialPageRoute(
              builder: (context) => new Login(
                notifyParent: args,
              ),
            );
            break;
          case '/signup':
            final args = settings.arguments;
            return MaterialPageRoute(
              builder: (context) => new Signup(
                notifyParent: args,
              ),
            );
            break;
          default:
            return null;
            break;
        }
      },
      routes: <String, WidgetBuilder>{
        '/search': (BuildContext context) => new Search(),
        '/basket': (BuildContext context) => new BasketPage(),
        '/reverse': (BuildContext context) => new ReversePage(),
        '/favorite': (BuildContext context) => new FavoritePage(),
        '/follow': (BuildContext context) => new FollowPage(),
        '/flight': (BuildContext context) => new FlightPage(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  User user = User.getUser();

  refresh(){
    setState(() { });
  }

  disconnect(){
    this.user = User.resetUser();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(user.token == null
              ? 'Bienvenue sur '
              : 'Bienvenue ${user.email.substring(0, user.email.indexOf('@'))}, sur' , style: TextStyle(color: Colors.white, fontSize: 22),),
              Text('\n'),
              Image.asset('assets/canoeFull.PNG')
            ]
          )
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(
                  color: Color(0xFFFF6A0F),
                ),
              child: Column(
                children: <Widget>[
                  Expanded(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text('CANOE', style: TextStyle(color: Colors.white),),
                    )
                  ),


                  if(this.user.token != null)
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: ListTile(
                        title: Text('Déconnexion', style: TextStyle(color: Colors.white)),
                        onTap: () => disconnect(),
                        ),
                      )
                    )
                ]
              )

            ),

            if (this.user.token == null)
              Container(
                child : Column(
                children: <Widget>[
                  ListTile(
                    title: Text('Connexion', style: TextStyle(color: Colors.white)),
                    onTap: () {
                      Navigator.of(context).pushNamed(
                          '/login',
                          arguments: refresh
                      );
                    },
                  ),
                  //if (this.user.token == null)
                  ListTile(
                      title: Text('Inscription', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed(
                            '/signup',
                            arguments: refresh
                        );
                      },
                    ),
                ],
                )
              )
            else
              Container(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text('Recherche', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/search');
                      },
                    ),
                    ListTile(
                      title: Text('Panier', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/basket');
                      },
                    ),
                    ListTile(
                      title: Text('Favoris', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/favorite');
                      },
                    ),
                    ListTile(
                      title: Text('Suivis', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/follow');
                      },
                    ),
                    ListTile(
                      title: Text('Recherche inversée', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/reverse');
                      },
                    ),
                    ListTile(
                      title: Text('Recherche de vols', style: TextStyle(color: Colors.white)),
                      onTap: () {
                        Navigator.of(context).pushNamed('/flight');
                      },
                    ),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
//
