import 'dart:convert';
import 'dart:io';

import 'package:canoemobile/entity/basket.dart';
import 'package:canoemobile/entity/product.dart';
import 'package:canoemobile/entity/search.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'dart:async';

import '../entity/user.dart';

class SearchService {
  User user = User.getUser();

  Future<Basket> searchBasket(String basket, bool aliExpress, Function(num) updateLoading) async {
    Search search = new Search();

    search.product_name = "";
    search.type = "basket";
    search.enable_aliExpress = aliExpress == true;
    search.file_path = null;
    search.basket = basket;

    String json = search.toString();

    try {
      await http.post(ConstantService.serviceUrl + '/search',
          headers: {
            'Authorization': this.user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("Data found");
      return await parseBasket(updateLoading);
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  Future<List<Product>> getFavorite(Function(num) updateLoading) async {
    User user = User.getUser();

    try {
      await http.post(ConstantService.serviceUrl + '/favorite/email', headers: {
        'Authorization': user.token,
        'Content-Type': 'application/json; charset=UTF-8'
      });
      print("Posted search");
      return await parseFavorite(updateLoading);
    } catch (e) {
      print('Server Exception :');
      print(e);
      return null;
    }
  }

  Future<List<Product>> getFollow(Function(num) updateLoading) async {
    User user = User.getUser();

    try {
      await http.post(ConstantService.serviceUrl + '/follow/email', headers: {
        'Authorization': user.token,
        'Content-Type': 'application/json; charset=UTF-8'
      });
      print("Posted search");
      return await parseFavorite(updateLoading);
    } catch (e) {
      print('Server Exception :');
      print(e);
      return null;
    }
  }

  Future<List<Product>> search(String value, bool aliExpress, Function(num) updateLoading) async {
    Search search = new Search();

    search.product_name = value;
    search.type = "single";
    search.enable_aliExpress = aliExpress == true;
    search.file_path = null;
    search.basket = "";

    String json = search.toString();

    try {
      await http.post(ConstantService.serviceUrl + '/search',
          headers: {
            'Authorization': this.user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("Data found");
      return await parseSingle(updateLoading);
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  // ignore: non_constant_identifier_names
  Future<List<Product>> reverse_search(File image, bool ailExpress, Function(num) updateLoading) async {
    print(image.readAsBytesSync().toString());

    Search search = new Search();

    search.product_name = "";
    search.type = "reverse";
    search.enable_aliExpress = ailExpress == true;
    search.file_path = "";
    search.basket = "";

    try {
      final length = await image.length();
      Map<String, String> headers = {'Authorization': this.user.token};
      final request = new http.MultipartRequest(
          'POST', Uri.parse(ConstantService.serviceUrl + '/reverse'));
      request.headers.addAll(headers);
      request.files.add(new http.MultipartFile(
          'image', image.openRead(), length,
          filename: 'image', contentType: new MediaType("image", "png")));
      request.fields.addAll(search.toMap());

      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Data found");
      print(response);
      return await parseReverse(updateLoading);
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  // ignore: non_constant_identifier_names
  Future<String> getProduct_name() async {
    try {
      var response = await http
          .get(ConstantService.serviceUrl + '/reverse/keyWord', headers: {
        'Authorization': this.user.token,
        'Content-Type': 'application/json; charset=UTF-8'
      });
      print("Data found");
      return utf8.decode(response.bodyBytes);
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  // ignore: missing_return
  Future<List<Product>> parseReverse(Function(num) updateLoading) async {
    return _fromJsonToSearchResult(utf8.decode((await getSearchResult(updateLoading)).bodyBytes));
  }

  // ignore: missing_return
  Future<List<Product>> parseSingle(Function(num) updateLoading) async {
    return _fromJsonToSearchResult(utf8.decode((await getSearchResult(updateLoading)).bodyBytes));
  }

  // ignore: missing_return
  Future<List<Product>> parseFavorite(Function(num) updateLoading) async {
    return _fromJsonToSearchResult(utf8.decode((await getSearchResult(updateLoading)).bodyBytes));
  }



  // ignore: missing_return
  Future<Basket> parseBasket(Function(num) updateLoading) async {
    return _fromJsonToBasket(utf8.decode((await getSearchResult(updateLoading)).bodyBytes));
  }

  Future<http.Response> getSearchResult(Function(num) updateLoading) async {

    bool resultFound = false;
    var response;
    while (resultFound == false) {
      try {
        response = await http.get(ConstantService.serviceUrl + '/search/percentage',
            headers: {'Authorization': this.user.token});

        Map<String, dynamic> map = json.decode(response.body);

        if (map['percentage'] != 100) {
          updateLoading(map['percentage']);
          resultFound = false;
        } else {
          response = await http.get(ConstantService.serviceUrl + '/search/token',
              headers: {'Authorization': this.user.token});
          resultFound = true;
          break;
        }
      } catch (e) {
        print('Server Exception!!!');
        print(e);
        return null;
      }

      await Future.delayed(const Duration(seconds: 1), () {});
    }

    return response;
  }

  Basket _fromJsonToBasket(String jsonBasket) {
    Map<String, dynamic> map = json.decode(jsonBasket);
    Basket basket = new Basket();
    if (map['searchResult'] != "") {
      Map<String, dynamic> response = json.decode(map['searchResult']);
      basket.site = response['site_name'];
      basket.price = response['price'];
      basket.products = new List<Product>();
      for (var i = 0; i < response['products'].length; i++) {
        basket.products.add(_fromObjectToProduct(response['products'][i]));
      }
      return basket;
    } else {
      return null;
    }
  }

  List<Product> _fromJsonToSearchResult(String jsonProducts) {
    Map<String, dynamic> map = json.decode(jsonProducts);
    if (map['searchResult'] != "") {
      List response = json.decode(map['searchResult']);
      List<Product> products = new List();
      for (var i = 0; i < response.length; i++) {
        products.add(_fromObjectToProduct(response[i]));
      }
      return products;
    } else {
      return null;
    }
  }

  Product _fromObjectToProduct(dynamic jsonProduct) {
    var product = new Product();
    product.name = jsonProduct['name'];
    product.price = jsonProduct['price'];
    product.description = jsonProduct['description'];
    product.image = jsonProduct['image'];
    product.availability = jsonProduct['availability'];
    product.rating = jsonProduct['rating'];
    product.url = jsonProduct['url'];
    product.site = jsonProduct['site'];
    product.user_rating = jsonProduct['user_rating'];
    product.price_notify = jsonProduct['price_notify'];
    return product;
  }
}
