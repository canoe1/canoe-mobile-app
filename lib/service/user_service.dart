import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../entity/user.dart';

class UserService {
  static final _serviceUrl = DotEnv().env['API_ADDRESS'] + ':' + DotEnv().env['API_PORT'];
  static final _headers = {'Content-Type': 'application/json; charset=UTF-8'};

  User user = User.getUser();


  Future<bool> login(User user) async {
    try {
      String json = _toJson(user);
      final response = await http.post(_serviceUrl + '/login',
          headers: _headers, body: json);

      this.user.token = response.headers['authorization'];
      print(this.user.token);
      return true;
    } catch (e) {
      print('Server Exception!!!');
      print(e);
      return false;
    }
  }

  Future<bool> signup(User user) async {
    try {
      String json = _toJson(user);
      final response = await http.post(_serviceUrl + '/signup',
          headers: _headers, body: json);
      this.user.email = null;
      this.user.password = null;
      return true;
    } catch (e) {
      print('Server Exception!!!');
      print(e);
      return false;
    }
  }

  Future<List<User>> getUsers() async {
    try {
      final response = await http
          .get(_serviceUrl + '/users', headers: {'Authorization': this.user.token});
      print(response);
      var c = _fromJsonToList(response.body);
      return c;
    } catch (e) {
      print('Server Exception!!!');
      print(e);
      return null;
    }
  }


  List<User> _fromJsonToList(String jsonUser) {
    List response = json.decode(jsonUser);
    List<User> users = new List();
    print(response);
    for (var i = 0; i < response.length; i++) {
      users.add(_fromJson(response[i].toString()));
    }
    print(users);
    return users;
  }

  User _fromJson(String jsonUser) {
    Map<String, dynamic> map = json.decode(jsonUser);
    var user = new User();
    user.email = map['email'];
    user.password = map['password'];
    return user;
  }

  String _toJson(User user) {
    var mapData = new Map();
    mapData["email"] = user.email;
    mapData["password"] = user.password;
    String jsonUser = json.encode(mapData);
    return jsonUser;
  }
}
