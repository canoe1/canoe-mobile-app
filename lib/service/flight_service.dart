
import 'dart:convert';

import 'package:canoemobile/entity/flight.dart';
import 'package:canoemobile/entity/flightParams.dart';
import 'package:canoemobile/entity/user.dart';

import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'dart:async';

import 'constant_service.dart';

class FlightService{
  User user = User.getUser();

  Future<List<Flight>> search(FlightParams flightParams, Function(num) updateLoading) async {
    flightParams.type = "flight";

    String json = flightParams.toString();

    print(json);

    try {
      var response = await http.post(ConstantService.serviceUrl + '/flight',
          headers: {
            'Authorization': this.user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("Data found");
      return await parseFlight(updateLoading);
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  // ignore: missing_return
  Future<List<Flight>> parseFlight( Function(num) updateLoading) async{
    return  _fromJsonToSearchResult(utf8.decode((await getSearchResult(updateLoading)).bodyBytes));
  }

  List<Flight> _fromJsonToSearchResult(String jsonFlights){
    Map<String, dynamic> map = json.decode(jsonFlights);
    if (map['searchResult'] != "") {
      List response = json.decode(map['searchResult']);
      List<Flight> flights = new List();
      for (var i = 0; i < response.length; i++) {
        flights.add(_fromObjectToFlight(response[i]));
      }
      return flights;
    } else {
      return null;
    }
  }

  Flight _fromObjectToFlight(dynamic jsonFlight) {
    var flight = new Flight();
    print(jsonFlight.toString());
    flight.logo_usr = jsonFlight['logo_usr'];
    flight.price = jsonFlight['price'];
    flight.d_hour = jsonFlight['d_hour'];
    flight.duration = jsonFlight['duration'];
    flight.d_airport = jsonFlight['d_airport'];
    flight.a_airport = jsonFlight['a_airport'];
    flight.row_url = jsonFlight['row_url'];
    flight.companies = new List<String>();
    for (var company in jsonFlight['companies']){
      flight.companies.add(company);
    }
    flight.stopover_list = new List<String>();
    for (var stopover in jsonFlight['stopover_list']){
      flight.stopover_list.add(stopover);
    }
    return flight;
  }

  Future<http.Response> getSearchResult(Function(num) updateLoading) async {

    bool resultFound = false;
    var response;
    while (resultFound == false) {
      try {
        response = await http.get(ConstantService.serviceUrl + '/search/percentage',
            headers: {'Authorization': this.user.token});

        Map<String, dynamic> map = json.decode(response.body);

        if (map['percentage'] != 100) {
          updateLoading(map['percentage']);
          resultFound = false;
        } else {
          response = await http.get(ConstantService.serviceUrl + '/flight/token',
              headers: {'Authorization': this.user.token});
          resultFound = true;
          break;
        }
      } catch (e) {
        print('Server Exception!!!');
        print(e);
        return null;
      }

      await Future.delayed(const Duration(seconds: 1), () {});
    }

    return response;
  }

}