import 'package:flutter_dotenv/flutter_dotenv.dart';

class ConstantService{
  static final _serviceUrl = DotEnv().env['API_ADDRESS'] + ':' + DotEnv().env['API_PORT'];
  static final _headers = {'Content-Type': 'application/json; charset=UTF-8'};

  static Map<String, String> get site => {
    "ebay": "Ebay",
    "amazon": "Amazon",
    "cdiscount": "C Discount",
    "boulanger": "Boulanger",
    "aliexpress": "Aliexpress"
  };

  static get serviceUrl => _serviceUrl;

  static get headers => _headers;
}