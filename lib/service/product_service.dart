
import 'package:canoemobile/entity/favorite.dart';
import 'package:canoemobile/entity/follow.dart';
import 'package:canoemobile/entity/user.dart';
import 'package:http/http.dart' as http;


import 'constant_service.dart';

class ProductService{

  Future<void> addFavorite(num rating, String url) async {
    User user = User.getUser();

    Favorite favorite = new Favorite();

    favorite.email = user.email;
    favorite.product_url = url;
    favorite.rating = rating;

    String json = favorite.toString();

    try {
      await http.post(ConstantService.serviceUrl + '/favorite',
          headers: {
            'Authorization': user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("favorite added");
      return;
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }

  Future<void>  deleteFavorite(String url) async {
    User user = User.getUser();

    String json = '{"url": "$url"}';

    try {
      await http.post(ConstantService.serviceUrl + '/favorite/delete',
          headers: {
            'Authorization': user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("favorite added");
      return;
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }


  Future<void> addFollow(num price_notify, String url) async {
    User user = User.getUser();

    Follow follow = new Follow();

    follow.email = user.email;
    follow.product_url = url;
    follow.price_notify = price_notify;

    String json = follow.toString();

    try {
      await http.post(ConstantService.serviceUrl + '/follow',
          headers: {
            'Authorization': user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("follow added");
      return;
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }


  Future<void>  deleteFollow(String url) async {
    User user = User.getUser();

    String json = '{"url": "$url"}';

    try {
      await http.post(ConstantService.serviceUrl + '/follow/delete',
          headers: {
            'Authorization': user.token,
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: json);
      print("follow added");
      return;
    } catch (e) {
      print('Server Exception !!!');
      print(e);
      return null;
    }
  }
}