import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../entity/user.dart';
import '../service/user_service.dart';

class Login extends StatefulWidget {
  Login({Key key, this.notifyParent}) : super(key: key);
  final Function() notifyParent;

  @override
  LoginPageState createState() => new LoginPageState();
}

class LoginPageState extends State<Login> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  User user = User.getUser();

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(backgroundColor: color, content: new Text(message)));
  }

  void getUsers() {
    var userService = new UserService();
    userService.getUsers().then((users) => print(users));
  }

  void _submitForm() {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      showMessage('Formulaire invalide!  Veuillez corriger.');
    } else {
      form.save(); //This invokes each onSaved event
      var userService = new UserService();
      userService.login(user).then((value) => this.user.token != null ? Navigator.of(context).pop() : print("Utilisateur non trouvé")).then((value) => widget.notifyParent());
    }
  }

  bool isEmail(String email){
    return !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Connexion", style: TextStyle(color: Colors.white),),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    style: TextStyle(color: Theme.of(context).cardColor),
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.person),
                      hintText: 'Saisissez votre e-mail',
                      labelText: 'Email',
                    ),
                    validator: (val) =>
                        isEmail(val) ? 'Email requis' : null,
                    onSaved: (val) => user.email = val,
                  ),
                  new TextFormField(
                    style: TextStyle(color: Theme.of(context).cardColor),
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp(r'[a-zA-Z0-9]'))
                    ],
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.lock),
                      hintText: 'Saisissez votre mot de passe',
                      labelText: 'Mot de passe',
                    ),
                    validator: (val) =>
                        val.isEmpty ? 'Mot de passe requis' : null,
                    onSaved: (val) => user.password = val,
                    obscureText: true,
                  ),
                  new Container(
                      padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                      child: new RaisedButton(
                        child: const Text('Se connecter', style: TextStyle(color: Colors.white),),
                        onPressed: _submitForm,
                      )),
                ],
              ))),
    );
  }
}
