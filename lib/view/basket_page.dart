import 'package:canoemobile/entity/basket.dart';
import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/search_service.dart';
import 'package:flutter/material.dart';

class BasketPage extends StatefulWidget {
  @override
  _BasketPageState createState() => _BasketPageState();
}

class _BasketPageState extends State<BasketPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  bool loading = false;

  num percentage = 0;

  bool checkedValue = false;

  List<TextEditingController> productNameControllers =
      new List<TextEditingController>();

  List<Widget> productsDisplay = new List<Widget>();

  List<Widget> resultView = new List<Widget>();

  List<Row> productNameFields = new List<Row>();

  List<String> products = new List<String>();

  Basket basket = new Basket();

  addProduct() async {
    TextEditingController productNameControllersTemp =
        new TextEditingController();
    productNameControllers.add(productNameControllersTemp);
    productNameFields.add(
      Row(
        children: <Widget>[
          Expanded(
            flex: 9,
            child: TextFormField(
              controller: productNameControllersTemp,
              decoration: const InputDecoration(
                icon: const Icon(Icons.mode_edit, color: Color(0xFF515151)),
                labelText: 'Nom du produit: ',
                hintText: 'Balais',
              ),
              validator: (val) =>
                  val.isEmpty ? 'Saisissez le nom d\'un produit' : null,
            ),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              //onTap: deleteProduct(productNameControllersTemp),
              onTap: () => { deleteProduct(productNameControllersTemp)},
              child: Icon(
                Icons.delete,
                size: 24,
                color: Colors.redAccent,
              ),
            ),
          )
        ],
      ),
    );

    updateProductsList();
    setState(() {});
  }

  deleteProduct(TextEditingController productNameController) {
    productNameFields.removeWhere((row) => ((row.children[0] as Expanded).child as TextFormField).controller  == productNameController);
    productNameControllers.removeWhere((controller) => productNameController == controller);
    setState(() {});
  }

  searchBasket() async {
    List<String> basketString = new List<String>();
    for (TextEditingController productNameController
        in productNameControllers) {
      basketString.add(productNameController.text);
    }

    final FormState form = _formKey.currentState;
    form.save();
    loading = true;
    setState(() {});
    var searchService = new SearchService();
    basket = await searchService.searchBasket(basketString.join(";"), checkedValue, updateLoading);
    updateView();
  }

  updateLoading(num percentage){
    this.percentage = percentage;
    setState(() {});
  }

  void updateView() {
    resultView = new List<Widget>();
    resultView.add(
      Column(
        children: <Widget>[
          new Text(
            basket.site,
            style: TextStyle(
              fontSize: 20,
              color: Colors.white
            ),
          ),
          new Text(
            "Total : "+ basket.price.toStringAsFixed(2) + " €",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white
            ),
          ),
          Column(
            children: createProductsView(),
          )
        ],
      )
    );
    loading = false;
    setState(() {});
  }

  List<Widget> createProductsView(){
    List<Widget> productsDisplay = new List<Widget>();
    for (var product in basket.products) {
      ProductPageArgument productPageArgument = new ProductPageArgument();
      productPageArgument.product = product;
      productPageArgument.notifyParent = null;
      productsDisplay.add(
        GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed('/product', arguments: productPageArgument);
          },
          child: Card(
            color: Theme.of(context).primaryColor,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    width: double.infinity,
                    child: Image.network(product.image,
                      width: MediaQuery.of(context).size.width * 0.2,
                      height: MediaQuery.of(context).size.width * 0.3,
                      fit: BoxFit.fitWidth,),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product.name,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white
                          ),
                        ),
                        Text(
                          ConstantService.site[product.site], style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Center(
                      child: Text(product.price.toStringAsFixed(2)+ ' €', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return productsDisplay;
  }

  void updateProductsList() {
    productsDisplay = new List<Widget>();
    for (var product in products) {
      productsDisplay.add(Card(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: new Text(
            product,
            style: TextStyle(
              fontSize: 18,
            ),),

      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Panier"),
      ),
      body: Container(
         margin: const EdgeInsets.only(top: 15.0),
        child: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.bottom -
              MediaQuery.of(context).padding.top -
              AppBar().preferredSize.height -
              20,
          child: ListView(
            physics: const AlwaysScrollableScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              if (!loading)
                Card(
                  child: Center(
                    child:Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      CheckboxListTile(
                        title: Text("Rechercher sur AliExpress"),
                        value: checkedValue,
                        onChanged: (newValue) {
                          setState(() {
                            checkedValue = newValue;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                      ),
                      new SafeArea(
                        top: false,
                        bottom: false,
                        child: new Form(
                          key: _formKey,
                          autovalidate: true,
                          child: SizedBox(
                            height: 150,
                            child: new ListView(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16.0),
                              children: <Widget>[
                                Column(
                                  children: productNameFields,
                                ),

                              ],
                            ),
                          ),
                        ),
                      ),
                      new Container(
                       // padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                        child: new RaisedButton(
                          child: const Text('Ajouter un produit', style: TextStyle(color: Colors.white),),
                          onPressed: addProduct,
                        ),
                      ),
                      new Container(
                        //padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                        alignment: Alignment(0.0, 0.0),
                        child: new RaisedButton(
                          child: const Text('Rechercher', style: TextStyle(color: Colors.white),),
                          onPressed: searchBasket,
                        ),
                      ),
                    ],
                  ),),
                ),
              if (!loading)
              Column(
                children: resultView,
              ),
              if (loading)
                Container(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height -
                        MediaQuery.of(context).padding.top -
                        MediaQuery.of(context).padding.bottom,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            '$percentage %',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor)),
                        ],
                      ),
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
