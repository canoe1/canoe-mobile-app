import 'package:canoemobile/entity/product.dart';
import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/entity/user.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/search_service.dart';
import 'package:flutter/material.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  final double inputSizedBoxHeight = 190.0;

  List<Widget> productsDisplay = new List<Widget>();

  final controller = ScrollController();

  bool checkedValue = false;

  bool loading = false;

  String searchValue = "";
  List<Product> products;

  num percentage = 0;

  User user = User.getUser();

  Future<void> search() async {
    final FormState form = _formKey.currentState;
    form.save();
    loading = true;
    setState(() {});
    var searchService = new SearchService();
    products = await searchService.search(searchValue, checkedValue, updateLoading);
    updateView();
  }

  updateLoading(num percentage){
    this.percentage = percentage;
    setState(() {});
  }

  void updateView() {
    productsDisplay = new List<Widget>();
    if (products == null) {
      return;
    }
    for (var product in products) {
      ProductPageArgument productPageArgument = new ProductPageArgument();
      productPageArgument.product = product;
      productPageArgument.notifyParent = null;
      productsDisplay.add(
        GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed('/product', arguments: productPageArgument);
          },
          child: Card(
            color: Theme.of(context).primaryColor,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    width: double.infinity,
                    child: Row(
                      children: <Widget>[
                        if (product.image != null)
                          Image.network(
                            product.image,
                            width: MediaQuery.of(context).size.width * 0.2,
                            height: MediaQuery.of(context).size.width * 0.3,
                            fit: BoxFit.fitWidth,
                          ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            product.name,
                            style: TextStyle(fontSize: 12, color: Colors.white),
                          ),
                          Text(
                            ConstantService.site[product.site],
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Center(
                      child: Text(
                        product.price.toStringAsFixed(2) + ' €',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    loading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Recherche"),
      ),
      body: ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            if (!loading)
              Card(
                child: new SafeArea(
                    top: false,
                    bottom: false,
                    child: new Form(
                        key: _formKey,
                        autovalidate: true,
                        child: SizedBox(
                          height: inputSizedBoxHeight,
                          child: new ListView(
                            physics: const AlwaysScrollableScrollPhysics(),
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            children: <Widget>[
                              CheckboxListTile(
                                title: Text("Rechercher sur AliExpress"),
                                value: checkedValue,
                                onChanged: (newValue) {
                                  setState(() {
                                    checkedValue = newValue;
                                  });
                                },
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                              ),
                              new TextFormField(
                                decoration: const InputDecoration(
                                  icon: const Icon(Icons.mode_edit,
                                      color: Color(0xFF515151)),
                                  labelText: 'Nom du produit: ',
                                  hintText: 'Balais',
                                ),
                                validator: (val) =>
                                    val.isEmpty ? '1 caractère minimum' : null,
                                onSaved: (val) => searchValue = val,
                              ),
                              new Container(
                                  padding: const EdgeInsets.only(
                                      left: 40.0, top: 20.0),
                                  child: new RaisedButton(
                                    child: const Text(
                                      'Rechercher',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: search,
                                  ))
                            ],
                          ),
                        ))),
              ),
            if (!loading)
              Card(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.bottom -
                      MediaQuery.of(context).padding.top -
                      inputSizedBoxHeight -
                      AppBar().preferredSize.height -
                      20,
                  child: new ListView(
                    children: productsDisplay,
                  ),
                ),
              ),
            if (loading)
              Container(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '$percentage %',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Theme.of(context).primaryColor)),
                      ],
                    ),
                  ),
                ),
              )
          ]),
    );
  }
}
