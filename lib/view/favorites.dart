import 'package:canoemobile/entity/product.dart';
import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/search_service.dart';
import 'package:flutter/material.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  bool loading = true;

  num percentage = 0;

  List<Widget> productsDisplay = new List<Widget>();

  List<Product> products;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getFavorite());
  }

  Future<void> getFavorite() async {
    loading = true;
    setState(() {});
    var searchService = new SearchService();
    products = await searchService.getFavorite(updateLoading);
    updateView();
  }

  updateLoading(num percentage) {
    this.percentage = percentage;
    setState(() {});
  }

  void deleteLocalFavorite(Product productToBeDeleted) {
    products.removeWhere((product) => product == productToBeDeleted);

    loading = true;
    setState(() {});
    updateView();
  }

  void updateView() {
    productsDisplay = new List<Widget>();
    for (var product in products) {
      ProductPageArgument productPageArgument = new ProductPageArgument();
      productPageArgument.product = product;
      productPageArgument.notifyParent = deleteLocalFavorite;
      productsDisplay.add(
        GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed('/product', arguments: productPageArgument);
          },
          child: Card(
            color: Theme.of(context).primaryColor,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        if (product.image != null)
                          Expanded(
                            flex: 1,
                            child: Image.network(
                              product.image,
                              width: MediaQuery.of(context).size.width * 0.2,
                              height: MediaQuery.of(context).size.width * 0.3,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product.name,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          ConstantService.site[product.site],
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: Text(
                            product.price.toStringAsFixed(2) + ' €',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        Center(
                          child: Text(
                            product.user_rating.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    loading = false;
    setState(() {});
  }

  void orderBy(String type) {
    switch (type) {
      case 'user_rating':
        this.products.sort((a, b) => b.user_rating.compareTo(a.user_rating));
        print(type);
        break;
      case 'price':
        this.products.sort((a, b) => a.price.compareTo(b.price));
        print(type);
        break;
      case 'website':
        this.products.sort((a, b) => a.site.compareTo(b.site));
        print(type);
        break;
      default:
        break;
    }
    this.updateView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Favoris"),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            if (loading)
              Container(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '$percentage %',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Theme.of(context).primaryColor)),
                      ],
                    ),
                  ),
                ),
              ),
            if (!loading && products.length != 0)
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new RaisedButton(
                        onPressed: () => orderBy('user_rating'),
                        child: Text("Note",
                            style: TextStyle(color: Theme.of(context).cardColor)),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new RaisedButton(
                        onPressed: () => orderBy('price'),
                        child: Text("Prix",
                            style: TextStyle(color: Theme.of(context).cardColor)),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new RaisedButton(
                        onPressed: () => orderBy('website'),
                        child: Text("Site",
                            style: TextStyle(color: Theme.of(context).cardColor)),
                      ),
                    ),
                  ),
                ],
              ),
            if (!loading && products.length != 0)
              Card(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.bottom -
                      MediaQuery.of(context).padding.top -
                      AppBar().preferredSize.height -
                      20,
                  child: new ListView(
                    children: productsDisplay,
                  ),
                ),
              ),
            if (!loading && products.length == 0)
              Text(
                "Aucun favoris trouvé",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
          ],
        ),
      ),
    );
  }
}
