import 'dart:io';

import 'package:canoemobile/entity/product.dart';
import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/search_service.dart';
import 'package:fab_circular_menu/fab_circular_menu.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ReversePage extends StatefulWidget {
  @override
  _ReversePageState createState() => _ReversePageState();
}

class _ReversePageState extends State<ReversePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  List<Widget> productsDisplay = new List<Widget>();

  final double inputSizedBoxHeight = 150.0;

  String searchValue = "";
  List<Product> products;
  String product_name;

  bool loading = false;
  bool checkedValue = false;
  File _image;

  num percentage = 0;

  final picker = ImagePicker();



  Future getImageWithGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      this._image = File(pickedFile.path);
    });
  }

  Future getImageWithCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera, imageQuality: 25);

    setState(() {
      this._image = File(pickedFile.path);
    });
  }

  Future<void> search() async {
    final FormState form = _formKey.currentState;
    form.save();
    loading = true;
    setState(() {});
    var searchService = new SearchService();
    products = await searchService.reverse_search(_image, checkedValue, updateLoading);
    String product_name = await searchService.getProduct_name();
    this.product_name = product_name;
    updateView();
  }

  updateLoading(num percentage){
    this.percentage = percentage;
    setState(() {});
  }

  void updateView() {
    productsDisplay = new List<Widget>();
    for (var product in products) {
      ProductPageArgument productPageArgument = new ProductPageArgument();
      productPageArgument.product = product;
      productPageArgument.notifyParent = null;
      productsDisplay.add(
        GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed('/product', arguments: productPageArgument);
          },
          child: Card(
            color: Theme.of(context).primaryColor,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    width: double.infinity,
                    child: Image.network(product.image,
                    width: MediaQuery.of(context).size.width * 0.2,
                    height: MediaQuery.of(context).size.width * 0.3,
                    fit: BoxFit.fitWidth,),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product.name,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white
                          ),
                        ),
                        Text(
                          ConstantService.site[product.site]
                          , style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Center(
                      child: Text(product.price.toStringAsFixed(2) + ' €', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    loading = false;
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text("Recherche Inversée"),
      ),
      body: ListView(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              if (!loading)
                Card(
                  child: new SafeArea(
                      top: false,
                      bottom: false,
                      child: new Form(
                          key: _formKey,
                          autovalidate: true,
                          child: SizedBox(
                            height: 150,
                              child: Row(
                                  children: <Widget>[
                                    new SizedBox(
                                      height: 200,
                                      width: 200,
                                      child: Center(
                                        child:_image == null
                                            ? Text('Aucune image sélectionnée', textAlign: TextAlign.center,
                                                style: TextStyle(color: Theme.of(context).backgroundColor),
                                            textDirection: TextDirection.ltr)
                                            : Image.file(_image
                                        ),

                                      ),

                                    ),
                                    if(_image != null)
                                          new SizedBox(
                                            height: 200,
                                            width: 150,
                                              child : Column(
                                                  children : <Widget> [
                                                    CheckboxListTile(
                                                      title: Text("Rechercher sur AliExpress", style: TextStyle(
                                                          fontSize: 12, color: Theme.of(context).backgroundColor
                                                      ),),
                                                      value: checkedValue,
                                                      onChanged: (newValue) {
                                                        setState(() {
                                                          checkedValue = newValue;
                                                        });
                                                      },
                                                      controlAffinity: ListTileControlAffinity.leading,
                                                    ),
                                                    Text('Nom du produit:', style: TextStyle(color: Theme.of(context).backgroundColor)),
                                                    Container(
                                                          child:SingleChildScrollView(
                                                              scrollDirection: Axis.vertical,
                                                            child:this.product_name == null
                                                                ? Text('Produit non reconnu.', textAlign: TextAlign.center,
                                                                style: TextStyle(color: Theme.of(context).backgroundColor))
                                                                : Text(this.product_name, style: TextStyle(color: Theme.of(context).backgroundColor)),
                                                          ),
                                                    ),
                                                    new RaisedButton(
                                                        child: const Text('Rechercher', style: TextStyle(color: Colors.white),),
                                                        onPressed: search),

                                                  ]
                                              )
                                      ),
                                  ]
                              )
                          ))),
                ),
              if (!loading)
                Card(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height -
                        MediaQuery.of(context).padding.bottom -
                        MediaQuery.of(context).padding.top -
                        inputSizedBoxHeight -
                        AppBar().preferredSize.height -
                        20,
                    child: new ListView(
                      children: productsDisplay,
                    ),
                  ),
                ),
              if (loading)
                Container(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height -
                        MediaQuery.of(context).padding.top -
                        MediaQuery.of(context).padding.bottom,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            '$percentage %',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                          CircularProgressIndicator(
                              valueColor: new AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor)),
                        ],
                      ),
                    ),
                  ),
                )
            ]),
      floatingActionButton: FabCircularMenu(
          children: <Widget>[
            IconButton(icon: Icon(Icons.add_a_photo, size: 50), onPressed: getImageWithCamera),
            IconButton(icon: Icon(Icons.add_photo_alternate, size: 50), onPressed: getImageWithGallery)
          ], fabOpenIcon: Icon(Icons.add),
        ringDiameter: MediaQuery.of(context).size.width * 0.8,
        ringColor: Color(0xFFC8C8C8),
        fabColor: Color(0xFFC8C8C8),
        fabOpenColor:Color(0xFFC8C8C8),
        fabCloseColor: Color(0xFFC8C8C8),
      )
    );
  }
}
