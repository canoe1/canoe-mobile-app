import 'package:canoemobile/entity/product.dart';
import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/search_service.dart';
import 'package:flutter/material.dart';

class FollowPage extends StatefulWidget {
  @override
  _FollowPageState createState() => _FollowPageState();
}

class _FollowPageState extends State<FollowPage> {
  bool loading = true;

  List<Widget> productsDisplay = new List<Widget>();

  List<Product> products;

  num percentage = 0;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => getFavorite());
  }

  Future<void> getFavorite() async {
    loading = true;
    setState(() {});
    var searchService = new SearchService();
    products = await searchService.getFollow(updateLoading);
    updateView();
  }

  updateLoading(num percentage){
    this.percentage = percentage;
    setState(() {});
  }

  void deleteLocalFollow(Product productToBeDeleted){
    products.removeWhere((product) => product == productToBeDeleted);

    loading = true;
    setState(() {});
    updateView();
  }

  void updateView() {
    productsDisplay = new List<Widget>();
    for (var product in products) {
      ProductPageArgument productPageArgument = new ProductPageArgument();
      productPageArgument.product = product;
      productPageArgument.notifyParent = deleteLocalFollow;
      productsDisplay.add(
        GestureDetector(
          onTap: () {
            Navigator.of(context)
                .pushNamed('/product', arguments: productPageArgument);
          },
          child: Card(
            color: Theme.of(context).primaryColor,
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    child: Row(
                      children: <Widget>[
                        if (product.image != null)
                          Expanded(
                            flex: 1,
                            child: Image.network(product.image,
                              width: MediaQuery.of(context).size.width * 0.2,
                              height: MediaQuery.of(context).size.width * 0.4,
                              fit: BoxFit.fitWidth,),
                          ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product.name,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white
                          ),
                        ),
                        Text(
                          ConstantService.site[product.site], style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    width: double.infinity,
                    child: Center(
                      child: Text(product.price.toStringAsFixed(2) + ' €', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    loading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text("Suivis"),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            if (loading)
              Container(
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.top -
                      MediaQuery.of(context).padding.bottom,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          '$percentage %',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                        CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Theme.of(context).primaryColor)),
                      ],
                    ),
                  ),
                ),
              ),
            if (!loading && products.length != 0)
              Card(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: SizedBox(
                  height: MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.bottom -
                      MediaQuery.of(context).padding.top -
                      AppBar().preferredSize.height -
                      20,
                  child: new ListView(
                    children: productsDisplay,
                  ),
                ),
              ),
            if (!loading && products.length == 0) Text("Aucun suivis trouvé", textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
          ],
        ),
      ),
    );
  }
}
