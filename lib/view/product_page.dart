import 'package:canoemobile/entity/productPageArgument.dart';
import 'package:canoemobile/service/constant_service.dart';
import 'package:canoemobile/service/product_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oktoast/oktoast.dart';

import 'package:url_launcher/url_launcher.dart';

class ProductPage extends StatefulWidget {
  final ProductPageArgument productPageArgument;

  const ProductPage({Key key, this.productPageArgument}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final GlobalKey<FormState> _favoriteFormKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> _followFormKey = new GlobalKey<FormState>();

  TextEditingController favoriteField = new TextEditingController();
  TextEditingController followField = new TextEditingController();

  String favoriteRating = "";

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  Future<void> addFavorite() async {
    var productService = new ProductService();


    if(favoriteField.text != "0"){
      await productService.addFavorite(
          double.parse(favoriteField.text), widget.productPageArgument.product.url);
      showToast(
        "Produit ajouté aux favoris",
        position: ToastPosition.bottom,
        radius: 13.0,
      );
    }else{
      await productService.deleteFavorite(widget.productPageArgument.product.url);
      showToast(
        "Produit supprimé des favoris",
        position: ToastPosition.bottom,
        radius: 13.0,
      );
      if(widget.productPageArgument.notifyParent != null){
        widget.productPageArgument.notifyParent(widget.productPageArgument.product);
      }
    }
  }

  Future<void> addFollow() async {
    var productService = new ProductService();


    if(followField.text != "0"){
      await productService.addFollow(
          double.parse(followField.text), widget.productPageArgument.product.url);
      showToast(
        "Produit ajouté aux suivis",
        position: ToastPosition.bottom,
        radius: 13.0,
      );
    }else{
      await productService.deleteFollow(widget.productPageArgument.product.url);
      showToast(
        "Produit supprimé des suivis",
        position: ToastPosition.bottom,
        radius: 13.0,
      );
      if(widget.productPageArgument.notifyParent != null){
        widget.productPageArgument.notifyParent(widget.productPageArgument.product);
      }
    }
  }

  void test(){
    print(widget.productPageArgument.product.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: Scaffold(
        appBar: new AppBar(
          title: new Text("Produit"),
        ),
        body: Container(

          child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(10),
                height: 400,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 4,
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            if (widget.productPageArgument.product.image != null)
                              Container(
                                constraints:
                                    BoxConstraints(minWidth: 100, maxWidth: 200),
                                child: SingleChildScrollView(
                                  child: Image.network(
                                    widget.productPageArgument.product.image,
                                    width: MediaQuery.of(context).size.width * 0.35,
                                    fit: BoxFit.fitWidth,
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 50, 15, 0),
                        child: Column(
                            children: <Widget>[

                          Container(
                            height: 200,
                            child: Card(
                              child:SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                child: Text(
                                  widget.productPageArgument.product.name,
                                  style: TextStyle(
                                    fontSize: 24, color: Theme.of(context).backgroundColor
                                  ),
                                  textAlign: TextAlign.center,
                                )
                              ),
                            )

                          ),
                              Container(
                                width: double.infinity,
                                child: Card(
                                  child: Text(
                                    widget.productPageArgument.product.price.toStringAsFixed(2) + ' €',
                                    style: TextStyle(
                                      fontSize: 24,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Card(
                                  child :InkWell(
                                    child: Text(
                                      "Voir sur site",
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Theme.of(context).cardColor
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    onTap: () => launch(widget.productPageArgument.product.url),
                                  ),
                                  color: Theme.of(context).backgroundColor,
                                ),
                              ),
                        ]
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          ConstantService.site[widget.productPageArgument.product.site],
                          style: TextStyle(
                            fontSize: 24,
                            color: Colors.white
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text(
                                widget.productPageArgument.product.description,
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: SizedBox(
                          height: 200,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 7,
                                      child: new Form(
                                        key: _favoriteFormKey,
                                        child: new TextFormField(
                                          style: TextStyle(color: Theme.of(context).cardColor),
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            WhitelistingTextInputFormatter.digitsOnly,
                                          ],
                                          controller: favoriteField..text = "${widget.productPageArgument.product.user_rating == null ||  widget.productPageArgument.product.user_rating == -1 ? "" : widget.productPageArgument.product.user_rating}",
                                          decoration: const InputDecoration(
                                              icon: const Icon(Icons.favorite, color: Colors.white,),
                                              hintText: '10',
                                              labelText: 'Note personnelle'),
                                          validator: (val) => !isNumeric(val)
                                              ? 'Saisissez un nombre'
                                              : null,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: new RaisedButton(
                                        child: const Text('Ajouter aux favoris', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                                        onPressed: addFavorite,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 7,
                                      child: new Form(
                                        key: _followFormKey,
                                        child: new TextFormField(
                                          style: TextStyle(color: Theme.of(context).cardColor),
                                          keyboardType: TextInputType.number,
                                          inputFormatters: <TextInputFormatter>[
                                            WhitelistingTextInputFormatter.digitsOnly,
                                          ],
                                          controller: followField..text = "${widget.productPageArgument.product.price_notify == null ||  widget.productPageArgument.product.price_notify == -1? "" : widget.productPageArgument.product.price_notify}",
                                          decoration: const InputDecoration(
                                              icon: const Icon(Icons.notifications, color: Colors.white,),
                                              hintText: '320.30',
                                              labelText: 'Prix à notifier'),
                                          validator: (val) => !isNumeric(val)
                                              ? 'Saisissez un nombre'
                                              : null,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: new RaisedButton(
                                        child: const Text('Suivre', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),),
                                        onPressed: addFollow,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      ),
    );
  }
}
