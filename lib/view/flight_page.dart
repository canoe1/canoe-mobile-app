import 'package:canoemobile/entity/flight.dart';
import 'package:canoemobile/entity/flightParams.dart';
import 'package:canoemobile/service/flight_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'package:url_launcher/url_launcher.dart';


class FlightPage extends StatefulWidget {
  @override
  _FlightPageState createState() => _FlightPageState();
}

class _FlightPageState extends State<FlightPage> {
  final double inputSizedBoxHeight = 375.0;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  List<Widget> flightDisplay = new List<Widget>();

  List<Flight> flights;

  bool loading = false;

  num percentage = 0.0;

  var _value;

  TextEditingController dateCtl = TextEditingController();

  final f = new DateFormat('dd/MM/yyyy');

  final FlightParams flightParams = new FlightParams();

  updateLoading(num percentage) {
    this.percentage = percentage;
    setState(() {});
  }

  Future<void> search() async {
    final FormState form = _formKey.currentState;
    form.save();

    loading = true;
    setState(() {});

    FlightService flightService = new FlightService();
    flights = await flightService.search(this.flightParams, updateLoading);
    print("search");
    updateView();
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2020),
        lastDate: new DateTime(2024)
    );
    if(picked != null) setState(() => _value = picked.toString()

    );
  }

  void updateView() {
    flightDisplay = new List<Widget>();
    if (flights == null) {
      return;
    }
    for (var flight in flights) {
      flightDisplay.add(GestureDetector(
        onTap: () => launch(flight.row_url),
        child: Card(
          color: Theme.of(context).primaryColor,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  width: double.infinity,
                  child: Row(
                    children: <Widget>[
                      if (flight.logo_usr != null)
                        Image.network(
                          flight.logo_usr,
                          width: MediaQuery.of(context).size.width * 0.2,
                          height: MediaQuery.of(context).size.width * 0.3,
                          fit: BoxFit.fitWidth,
                        ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Départ : " + flight.d_airport,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          "Arrivée : " + flight.a_airport,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          "Heure de départ : " + flight.d_hour,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          "Durée de trajet : " + flight.duration,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          "Compagnie(s)  : " + flight.companies.join(", "),
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                        Text(
                          "Escale(s)  : " + flight.stopover_list.join(", "),
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  width: double.infinity,
                  child: Center(
                    child: Text(
                      flight.price,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ));
    }
    print("after updateView");
    loading = false;
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Vols"),
      ),
      body: new ListView(
        children: <Widget>[
          if (!loading)
            new SafeArea(
              top: false,
              bottom: false,
              child: new Form(
                key: _formKey,
                autovalidate: true,
                child: Container(
                  height: (MediaQuery.of(context).size.height -
                      MediaQuery.of(context).padding.bottom -
                      MediaQuery.of(context).padding.top -
                      AppBar().preferredSize.height) / 2 ,
                  child: Card(
                    child: Center(
                      child: new ListView(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      children: <Widget>[
                        new TextFormField(
                          decoration: const InputDecoration(
                            icon:
                                const Icon(Icons.mode_edit, color: Color(0xFF515151)),
                            labelText: 'Aéroport d\'origine: ',
                            hintText: 'Paris',
                          ),
                          validator: (val) =>
                              val.isEmpty ? '1 caractère minimum' : null,
                          onSaved: (val) => flightParams.origin_airport = val,
                        ),
                        new TextFormField(
                          decoration: const InputDecoration(
                            icon:
                                const Icon(Icons.mode_edit, color: Color(0xFF515151)),
                            labelText: 'Aéroport d\'arrivée: ',
                            hintText: 'Los Angeles',
                          ),
                          validator: (val) =>
                              val.isEmpty ? '1 caractère minimum' : null,
                          onSaved: (val) =>
                              flightParams.destination_airport = val,
                        ),
                        new TextFormField(

                          controller: dateCtl,
                          decoration: const InputDecoration(
                            icon:
                                const Icon(Icons.mode_edit, color: Color(0xFF515151)),
                            labelText: 'Date de départ: ',
                            hintText: '10/09/2020',
                          ),
                          onTap: () async{
                            FocusScope.of(context).requestFocus(new FocusNode());
                            DateTime date = DateTime(1900);
                            //_selectDate();
                            date = await showDatePicker(
                              context: context,
                              initialDate: new DateTime.now(),
                              firstDate: new DateTime(2020),
                              lastDate: new DateTime(2024));
                            dateCtl.text = f.format(date);
                          },

                          onSaved: (val) => flightParams.departure_date = val,
                        ),
                        new TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            WhitelistingTextInputFormatter.digitsOnly,
                          ],
                          decoration: const InputDecoration(
                            icon:
                                const Icon(Icons.mode_edit, color: Color(0xFF515151)),
                            labelText: 'Nombre de passagers: ',
                            hintText: '2',
                          ),
                          validator: (val) =>
                              val.isEmpty ? '1 caractère minimum' : null,

                          onSaved: (val) =>
                              flightParams.passenger = num.parse(val),
                        ),
                        new Container(
                          padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                          child: new RaisedButton(
                            child: const Text(
                              'Rechercher',
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: search,
                          ),
                        )
                      ],
                    ),
                ),
              ))
              ),
            ),
          if (!loading)
            Card(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: SizedBox(
                height: (MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.bottom -
                    MediaQuery.of(context).padding.top -
                    AppBar().preferredSize.height) / 2 - 20,
                child: new ListView(
                  children: flightDisplay,
                ),
              ),
            ),
          if (loading)
            Container(
              child: SizedBox(
                height: MediaQuery.of(context).size.height -
                    MediaQuery.of(context).padding.top -
                    MediaQuery.of(context).padding.bottom,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '$percentage %',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Theme.of(context).primaryColor)),
                    ],
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
