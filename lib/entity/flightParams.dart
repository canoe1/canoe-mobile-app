class FlightParams{
  String origin_airport;
  String destination_airport;
  String departure_date;
  String type;
  int passenger;

  @override
  String toString() {
    return '{"origin_airport": "$origin_airport", "type": "$type", "destination_airport": "$destination_airport", "departure_date": "$departure_date", "passenger": $passenger}';
  }
}