class Product {
  String name;
  num price;
  String description;
  String image;
  String availability;
  String rating;
  String url;
  String site;
  num user_rating;
  num price_notify;

  Product();


  @override
  String toString() {
    return 'Product{name: $name, price: $price, description: $description, image: $image, availability: $availability, rating: $rating, url: $url, site: $site, user_rating: $user_rating, price_notify: $price_notify}';
  }

  Product.fromProduct(Product product){
    this.name = product.name;
    this.price = product.price;
    this.description = product.description;
    this.image = product.image;
    this.availability = product.availability;
    this.rating = product.rating;
    this.url = product.url;
    this.site = product.site;
    this.user_rating = product.user_rating;
    this.price_notify = product.price_notify;
  }
}