import 'dart:io';

import 'dart:typed_data';

class Search {
  int id;
  String type;
  String product_name;
  String basket;
  String file_path;
  bool enable_aliExpress;

  @override
  String toString() {
    return '{"type": "$type", "product_name": "$product_name", "basket": "$basket", "file_path": "$file_path", "enable_aliExpress": "$enable_aliExpress"}';
  }

  Map<String, String> toMap(){
    return {"postSearchDTO":'{"type": "$type", "product_name": "$product_name", "basket": "$basket", "file_path": "$file_path", "enable_aliExpress": "$enable_aliExpress"}'};
  }
}