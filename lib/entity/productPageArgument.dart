import 'package:canoemobile/entity/product.dart';

class ProductPageArgument{
  Product product;
  Function(Product product) notifyParent;
}