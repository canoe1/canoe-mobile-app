import 'package:canoemobile/entity/product.dart';

class Basket {
  String site;
  num price;
  List<Product> products;
}