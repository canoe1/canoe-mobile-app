class User {
  int id;
  String email;
  String password;
  String token;

  static User user;


  static User getUser(){
    if(user == null){
      user = new User();
    }
    return user;
  }

  static User resetUser(){
    user = new User();
    return user;
  }
}