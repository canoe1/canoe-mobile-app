class Flight{
  String logo_usr;
  String price;
  String d_hour;
  String duration;
  String d_airport;
  String a_airport;
  String row_url;
  List<String> companies;
  List<String> stopover_list;

  @override
  String toString() {
    return 'Flight{logo_usr: $logo_usr, price: $price, d_hour: $d_hour, duration: $duration, d_airport: $d_airport, a_airport: $a_airport, companies: $companies, stopover_list: $stopover_list}';
  }
}